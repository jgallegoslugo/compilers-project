  /**
 * Represents the symbol table of the compiler.
 */
import java.util.*;
public class SymbolTable {

	public static final Symbol levelDivider = Symbol.symbol();

	public int level = 0;
	
	
	LinkedList<Symbol> scopeList = new LinkedList<Symbol>();

  public SymbolTable() {
  	scopeList.add(levelDivider);
  	scopeList.add(Symbol.symbol("int", Symbol.Kind.TYPE));
    scopeList.add(Symbol.symbol("char", Symbol.Kind.TYPE));
    scopeList.add(Symbol.symbol("string", Symbol.Kind.TYPE));
    scopeList.add(Symbol.symbol("void", Symbol.Kind.TYPE));
    scopeList.add(levelDivider);
  }

  public boolean addSymbol(Symbol s) {

  	if(locate(s, true) == null)
  		return scopeList.add(s);
  	
  	return false;
  }

  /**
   * Returns the symbol with the given name and kind in the current scope,
   * or null if such a symbol does not exist.
   */
  public Symbol getSymbol(String name, Symbol.Kind kind) {

  	return locate(Symbol.symbol(name, kind), false);
  }

  public Symbol locate(Symbol symbol, boolean fromAddSymbol)
  {
  	Iterator<Symbol> i = scopeList.descendingIterator();

  	while(i.hasNext())
  	{
  		Symbol next = i.next();
  		if(fromAddSymbol && next == levelDivider)
  		{
  			return null;
  		}
  		if(equals(next, symbol))
  			return next;
  	}
  	
  	return null;
  }
  
  public boolean equals(Symbol inList, Symbol toAdd)
  {
  	if(inList == levelDivider) //if the current scope is empty
  		return false;
  	
  	//if the names are the same, run through other possible tests
  	if(checkName(inList, toAdd))
  		return checkKind(inList, toAdd);
  	
  	//else there is no need to check
  	return false;
  }

  private boolean checkName(Symbol one, Symbol two)
  {
    return one.getName().equals(two.getName());
  }
  
  private boolean checkKind(Symbol one, Symbol two)
  {  	
  	Symbol.Kind procedure = Symbol.Kind.PROCEDURE;
  	
  	if(one.getKind().equals(procedure) ^ two.getKind().equals(procedure))
    	return false;
    
  	return true;
  }

  @SuppressWarnings("unused")
  private boolean checkType(Symbol one, Symbol two)
  {
    return one.getType().equals(two.getType());
  }

  public void newScope() {
  	level++;
    scopeList.add(levelDivider);
  }

  public void exitScope() {
  	level--;
  	
  	while(scopeList.getLast() != levelDivider)
  	{
  		scopeList.removeLast();
  	}
  	
  	//remove the last divider
  	scopeList.removeLast();
  }
  
  public int currentScope()
  {
  	return level;
  }
  

  @Override
  public String toString() {
    // Print the symbol table according to insertion order
    StringBuilder res = new StringBuilder("***** Symbol Table Contents *****\n");

    int scope = 0;
    Iterator<Symbol> iterator = scopeList.iterator();
    
    while(iterator.hasNext())
    {
    	Symbol next = iterator.next();
    	
    	if(next == levelDivider)
    		scope++;
    	else if(scope > 2)
    	{
    		for(int i = 2; i < scope; i++)
    			res.append("-->");
    		  res.append(next + "\n");
    	}
    	else
    		res.append(next + "\n");
    }
    
    return res.toString();
  }
}
