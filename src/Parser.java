import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//import Type.ArrayType;

public class Parser {

  public enum ErrorType { UNDEFINED_ERROR, REDEFINED_ERROR, MISMATCH_ERROR, ARITY_ERROR; }

  private Scanner scanner;
  private SymbolTable symbolTable;

  private StringBuilder parseTreeBuffer;
  private int recursionDepth;
  private List<String> errors;

  private boolean doSemanticChecks;
  
  private Token returnToken = null;

  public Parser(boolean doSemanticChecks) {
    this.doSemanticChecks = doSemanticChecks;
  }

  public Parser() {
    this(true);
  }

  public void parse(String sourceFilename) throws ParserException {
    // (Re-)initialize fields
    parseTreeBuffer = new StringBuilder();
    recursionDepth = 1;   // to match test cases, should really be 0
    errors = new ArrayList<String>();

    scanner = new Scanner(sourceFilename);
    scanner.next();
    symbolTable = new SymbolTable();
    initSymbolTable();

    // Start the parsing
    program();
  }

  // ---------- Methods for managing parse tree ----------
  private void enterRule(NonTerminal nt) throws ParserException {
    if (! have(nt)) printError(nt);

    StringBuilder lineData = new StringBuilder();
    for (int i = 0; i < recursionDepth; i++) lineData.append("   ");
    lineData.append(nt.name);
    parseTreeBuffer.append(lineData).append("\n");
    recursionDepth++;
  }

  private void exitRule(NonTerminal nt) {
    recursionDepth--;
  }

  public String getParseTree() {
    return parseTreeBuffer.toString();
  }

  // ---------- Methods for error reporting ----------
  private void addTestCaseErrorMessage() {
    // For comparing output: please use this format as it will be used to compare output results.
    if (doSemanticChecks) {
      StringBuilder errorMessage = new StringBuilder();
      if (scanner.getTokenKind() == TokenKind.ERROR) {
        errorMessage.append("Syntax Error (Scanner): SCAN_ERROR on symbol " + scanner.getLexeme());
      } else {
        errorMessage.append("Syntax Error (Parser):  on symbol " + scanner.getLexeme());
      }
      errorMessage.append("\n");
      errorMessage.append("Error Found on Line Number: ").append(scanner.getLineNum()).append("\n");
      errorMessage.append(symbolTable);
      errors.add(errorMessage.toString());
    } else {
      errors.add("Error: " + scanner.getTokenKind() + "  " + scanner.getLexeme());
    }
  }

  private void printError(TokenKind expectedToken) throws ParserException {
    // We are terminating the parse here, but better compilers will do error
    // recovery and keep going to find more syntax errors.
    addTestCaseErrorMessage();
    String errorMessage = "Error: found " + scanner.getTokenKind() + " (" + scanner.getLexeme() + ")"
                        + " but expected: " + expectedToken;
    throw new ParserException(scanner.getLineNum(), scanner.getCharPos(), errorMessage);
  }

  private void printError(NonTerminal nt) throws ParserException {
    addTestCaseErrorMessage();
    String errorMessage = "Error: found " + scanner.getTokenKind() + " (" + scanner.getLexeme() + ")"
                        + " but expected: " + nt + " with " + nt.firstSet;
    throw new ParserException(scanner.getLineNum(), scanner.getCharPos(), errorMessage);
  }

  private void printError(ErrorType errorType, Token nameToken) throws ParserException {
    String errorMessage = "SEMANTIC ERROR: " + errorType + " on symbol " + nameToken.getLexeme();
    StringBuilder outputError = new StringBuilder();
    outputError.append(errorMessage).append("\n");
    outputError.append("Error Found on Line Number: " + nameToken.getLineNum()).append("\n");
    outputError.append(symbolTable);
    errors.add(outputError.toString());
    throw new ParserException(nameToken.getLineNum(), nameToken.getCharPos(), errorMessage);
  }

  private void printError(ErrorType errorType, Token nameToken, String message)
      throws ParserException {
    String errorMessage = "SEMANTIC ERROR: " + errorType + " on symbol " + nameToken.getLexeme();
    StringBuilder outputError = new StringBuilder();
    outputError.append(errorMessage).append("\n");
    outputError.append("Error Found on Line Number: " + nameToken.getLineNum())
           .append(", Char Position: " + nameToken.getCharPos()).append("\n");
    if (message != null) outputError.append(message).append("\n");
    outputError.append(symbolTable);
    errors.add(outputError.toString());
    throw new ParserException(nameToken.getLineNum(), nameToken.getCharPos(), errorMessage);
  }

  public List<String> getErrors() {
    return errors;
  }

  // ---------- Symbol Table management methods ----------
  private void initSymbolTable() {
    symbolTable = new SymbolTable();
  }

  private void tryDeclareSymbol(Token nameToken, Symbol symbol) throws ParserException {
    if(!symbolTable.addSymbol(symbol))
    {
    	this.printError(ErrorType.REDEFINED_ERROR, nameToken);
    }
  }

  private Symbol tryResolveSymbol(Token nameToken, Symbol.Kind kind) throws ParserException {
    Symbol symbol = symbolTable.getSymbol(nameToken.getLexeme(), kind);
  	if( symbol == null)
    	this.printError(ErrorType.UNDEFINED_ERROR, nameToken);
    
    	return symbol;
  }

  public SymbolTable getSymbolTable() {
    return symbolTable;
  }

  // ---------- Helper methods ----------
  private boolean have(TokenKind tokenKind) {
    return scanner.getTokenKind() == tokenKind;
  }

  private boolean have(NonTerminal nt) {
    return nt.firstSet.contains(scanner.getTokenKind());
  }

  private boolean accept(TokenKind tokenKind) {
    if (have(tokenKind)) {
      scanner.next();
      return true;
    }
    return false;
  }

  private boolean accept(NonTerminal nt) {
    if (have(nt)) {
      scanner.next();
      return true;
    }
    return false;
  }

  private Token acceptRetrieve(NonTerminal nt) throws ParserException
  {
    Token res = scanner.getToken();
    expect(nt);
    return res;
  }
  private boolean expect(TokenKind tokenKind) throws ParserException {
    if (accept(tokenKind)) return true;
    printError(tokenKind);
    return false;
  }

  private boolean expect(NonTerminal nt) throws ParserException {
    if (accept(nt)) return true;
    printError(nt);
    return false;
  }

  private Token expectRetrieve(TokenKind tokenKind) throws ParserException {
    Token res = scanner.getToken();
    expect(tokenKind);
    return res;
  }

  private Token expectRetrieve(NonTerminal nt) throws ParserException {
    Token res = scanner.getToken();
    expect(nt);
    return res;
  }

  // selector := IDENT { "[" expression "]" }
  private Type selector() throws ParserException {
    enterRule(NonTerminal.SELECTOR);

    Token token = expectRetrieve(TokenKind.IDENT);
    Type type;
    Symbol symbol = this.tryResolveSymbol(token, Symbol.Kind.VAR);
    int depth = 0;
    
    type = symbol.getType();
   
    if(!(type instanceof Type.ArrayType) && have(TokenKind.L_BRACKET))
    {	
 		 printError(ErrorType.MISMATCH_ERROR, token, "Invalid array reference: base = " + type + ", index = " +type);
 	 	} 
    while (accept(TokenKind.L_BRACKET)) {
    	Type arg = expression();
    	
      if(!arg.toString().equals("int"))
        printError(ErrorType.MISMATCH_ERROR, token, "Invalid array reference: base = " + getBaseType(symbol, depth) + ", index = " + arg);
      expect(TokenKind.R_BRACKET);
      depth++;
    }
    exitRule(NonTerminal.SELECTOR);
    
    if(symbol.getType() instanceof Type.ArrayType)
      return getBaseType(symbol, depth);
    else
      return symbol.getType();
  }

  //returns params to be checked in print and possibly input
  // parameters := IDENT { "," IDENT }
  private ArrayList<Symbol> parameters(Symbol symbolList) throws ParserException {
    enterRule(NonTerminal.PARAMETERS);

    Symbol symbol;
    ArrayList<Symbol> passParams = new ArrayList<Symbol>();
    
    do {
      Token token = expectRetrieve(TokenKind.IDENT);
      symbol = this.tryResolveSymbol(token, Symbol.Kind.VAR);
      passParams.add(symbol);
      
    } while (accept(TokenKind.COMMA));

    exitRule(NonTerminal.PARAMETERS);
    
    return passParams;
  }

  // condition := "(" expression relop expression ")"
  private void condition() throws ParserException {
    enterRule(NonTerminal.CONDITION);

    expect(TokenKind.L_PAREN);
    Type left = expression();
    Token op = expectRetrieve(NonTerminal.RELOP);
    Type right = expression();
    
    if(!left.equals(right))
    	printError(ErrorType.MISMATCH_ERROR, op, "Invalid operator usage: " + op.getLexeme() + ", " + left + ", " + right);
    else if((left instanceof Type.ArrayType) || (right instanceof Type.ArrayType))
    	printError(ErrorType.MISMATCH_ERROR, op, "Invalid operator usage: " + op.getLexeme() + ", " + left + ", " + right);
    expect(TokenKind.R_PAREN);

    exitRule(NonTerminal.CONDITION);
  }

  // procedureCall := "::" IDENT "(" [ parameters ] ")"
  private Type procedureCall() throws ParserException {
    enterRule(NonTerminal.PROCEDURE_CALL);

    expect(TokenKind.COLON);
	  expect(TokenKind.COLON);
    Token token = expectRetrieve(TokenKind.IDENT);
    
    Symbol symbol = this.tryResolveSymbol(token, Symbol.Kind.PROCEDURE);
    ArrayList<Symbol> formalParams = symbol.getParams();
    ArrayList<Symbol> args = new ArrayList<Symbol>();
    expect(TokenKind.L_PAREN);
    if (have(NonTerminal.PARAMETERS))
    {
    	args = parameters(symbol);
    }
    
    String message = "Invalid call: " + paramFormat(symbol, formalParams) + " with " + paramFormat(null, args);
    if(formalParams.size() != args.size())
    {
    	
    	printError(ErrorType.ARITY_ERROR, token, message);
    }
    else
    {
    	for(int i = 0; i < formalParams.size(); i++)
    	{
    		if(!formalParams.get(i).getType().equals(args.get(i).getType()))
    			printError(ErrorType.MISMATCH_ERROR, token, message);
    	}
    }
    expect(TokenKind.R_PAREN);
    
    exitRule(NonTerminal.PROCEDURE_CALL);

    return symbol.getType();
  }
  private String paramFormat(Symbol name, ArrayList<Symbol> list)
  {
  	StringBuilder message = new StringBuilder();
  	
  	if(name != null)
  		message.append(name.getName()).append("(").append(list.get(0).getType());
  	else
  		message.append("(").append(list.get(0).getType());
  	
  	for(int i = 1; i < list.size(); i++)
  		message.append(", ").append(list.get(i).getType());
  	
  	message.append(")");
  	
  	return message.toString();	
  }
  // assignment := selector "=" ( expression | string_literal ) ";"
  private void assignment() throws ParserException {
    enterRule(NonTerminal.ASSIGNMENT);

    Type left = selector();
   
    Token op = expectRetrieve(TokenKind.ASSIGN);
    if (have(NonTerminal.EXPRESSION)) 
    {
     Type right = expression();
     if((left instanceof Type.ArrayType) || (right instanceof Type.ArrayType))
     {
    	 printError(ErrorType.MISMATCH_ERROR, op, "Invalid assignment: " + left + " <- " + right);
     }
     if(!left.equals(right))
    	 printError(ErrorType.MISMATCH_ERROR, op, "Invalid assignment: " + left + " <- " + right);//this is what I just added
    }
    else expect(TokenKind.STRING_LITERAL);
    expect(TokenKind.SEMICOLON);

    exitRule(NonTerminal.ASSIGNMENT);
  }

  private void input() throws ParserException {
    enterRule(NonTerminal.INPUT);

    Token token = expectRetrieve(TokenKind.INPUT);
    expect(TokenKind.L_PAREN);
    ArrayList<Symbol> args = parameters(null);
    
    for(Symbol symbol: args)
    {
    	if(symbol.getType() instanceof Type.ArrayType)
    		printError(ErrorType.MISMATCH_ERROR, token, "Invalid argument: " + symbol.getName() + ", " + symbol.getType());
    }
    
    expect(TokenKind.R_PAREN);
    expect(TokenKind.SEMICOLON);

    exitRule(NonTerminal.INPUT);
  }
  
  // output := "print" "(" parameters ")" ";"
  private void output() throws ParserException {
    enterRule(NonTerminal.OUTPUT);
    
    Token token = expectRetrieve(TokenKind.PRINT);
    expect(TokenKind.L_PAREN);
    ArrayList<Symbol> args = parameters(null);
    
    for(Symbol symbol: args)
    {
    	if(symbol.getType() instanceof Type.ArrayType)
    		printError(ErrorType.MISMATCH_ERROR, token, "Invalid argument: " + symbol.getName() + ", " + symbol.getType());
    }
    expect(TokenKind.R_PAREN);
    expect(TokenKind.SEMICOLON);

    exitRule(NonTerminal.OUTPUT);
  }

  // ifStatement := "if" condition "{" statementSequence "}"
  //                [ "else" "{" statementSequence "}" ]
  private Type ifStatement() throws ParserException {
    enterRule(NonTerminal.IF_STATEMENT);
    Type type = null;
    expect(TokenKind.IF);
    condition();
    expect(TokenKind.L_BRACE);
    type = statementSequence();
    expect(TokenKind.R_BRACE);
    if (accept(TokenKind.ELSE)) {
      expect(TokenKind.L_BRACE);
      type = statementSequence();
      expect(TokenKind.R_BRACE);
    }

    exitRule(NonTerminal.IF_STATEMENT);
    return type;
  }

  // whileStatement := "while" condition "{" statementSequence "}"
  private Type whileStatement() throws ParserException {
    enterRule(NonTerminal.WHILE_STATEMENT);
    Type type = null;
    expect(TokenKind.WHILE);
    condition();
    expect(TokenKind.L_BRACE);
    type = statementSequence();
    expect(TokenKind.R_BRACE);

    exitRule(NonTerminal.WHILE_STATEMENT);
    return type;
  }

  // returnStatement := "return" expression ";"
  private Type returnStatement() throws ParserException {
    enterRule(NonTerminal.RETURN_STATEMENT);

    returnToken = expectRetrieve(TokenKind.RETURN);
    Type type = expression();
    expect(TokenKind.SEMICOLON);

    exitRule(NonTerminal.RETURN_STATEMENT);
    return type;
  }

  // procedureStatement := procedureCall ";"
  private void procedureStatement() throws ParserException {
    enterRule(NonTerminal.PROCEDURE_STATEMENT);

    procedureCall();
    expect(TokenKind.SEMICOLON);

    exitRule(NonTerminal.PROCEDURE_STATEMENT);
  }

  // statement := assignment | input | output | ifStatement |
  //              whileStatement | returnStatement | procedureStatement
  private Type statement() throws ParserException {
    enterRule(NonTerminal.STATEMENT);

    if (have(NonTerminal.ASSIGNMENT)) assignment();
    else if (have(NonTerminal.INPUT)) input();
    else if (have(NonTerminal.OUTPUT)) output();
    else if (have(NonTerminal.IF_STATEMENT)) return ifStatement();
    else if (have(NonTerminal.WHILE_STATEMENT)) return whileStatement();
    else if (have(NonTerminal.RETURN_STATEMENT)) return returnStatement();
    else if (have(NonTerminal.PROCEDURE_STATEMENT)) procedureStatement();
    else expect(NonTerminal.STATEMENT);

    exitRule(NonTerminal.STATEMENT);
    return null;
  }

  // statementSequence := statement { statement }
  private Type statementSequence() throws ParserException {
    enterRule(NonTerminal.STATEMENT_SEQUENCE);
    Type type;
    do {
      type = statement();
      if(type != null)
      	return type;
    } while (have(NonTerminal.STATEMENT));

    exitRule(NonTerminal.STATEMENT_SEQUENCE);
    return type;
  }

  // factor := selector | NUMBER | procedureCall | "(" expression ")"
  private Type factor() throws ParserException {
    enterRule(NonTerminal.FACTOR);
    
    if (have(NonTerminal.SELECTOR)) return selector();
    else if (accept(TokenKind.NUMBER)) {return Type.newPrimitiveType("int");}
    else if (have(NonTerminal.PROCEDURE_CALL)) return procedureCall();
    else if (accept(TokenKind.L_PAREN)) {
      Type type = expression();
      expect(TokenKind.R_PAREN);
      return type;
    } else expect(NonTerminal.FACTOR);

    exitRule(NonTerminal.FACTOR);
    return null;
  }

  // term := factor { op1 factor }
  private Type term() throws ParserException {
    enterRule(NonTerminal.TERM);

    Type left = factor();
    while (have(NonTerminal.OP1))
    {
    	Token op = acceptRetrieve(NonTerminal.OP1);
    	Type right = factor();
    	
    	if(left.toString().equals("int") && right.toString().equals("int"))//div and mult can only be int op int
    	{
    			
    	}
    	else
    		printError(ErrorType.MISMATCH_ERROR, op, "Invalid operator usage: " + op.getTokenKind().defaultLexeme + ", " +left.toString() + ", " + right.toString() );
    
    }
    exitRule(NonTerminal.TERM);
    
    return left;
  }

  // expression := term { op2 term }
  private Type expression() throws ParserException {
    enterRule(NonTerminal.EXPRESSION);

    Type string = Type.newPrimitiveType("string");
    Type left = term();
    while (have(NonTerminal.OP2)) 
    {
    	Token op = acceptRetrieve(NonTerminal.OP2);
    	Type right = term();
    	if(op.getLexeme().equals("-"))
    	{
    		if(!left.equals(right))
    			printError(ErrorType.MISMATCH_ERROR, op, "Invalid operator usage: " + op.getLexeme() + ", " + left + ", " + right );
    	}
    	else if(op.getLexeme().equals("+"))
    	{
    		if(left.equals(right))
    		{
    			//do nothing. equals types are okay.
    		}
    		else if(left.equals(string) || right.equals(string))
    		{
    			left = string;
    		}
    		else if((left instanceof Type.ArrayType) || (right instanceof Type.ArrayType))
    		{
    			printError(ErrorType.MISMATCH_ERROR, op, "Invalid operator usage: " + op.getLexeme() + ", " + left + ", " + right);
    		}
    	}
    }
    exitRule(NonTerminal.EXPRESSION);
    
    return left;
  }

  // retType := "int" | "string" | "void" | "char"
  private Type retType() throws ParserException {
    enterRule(NonTerminal.RET_TYPE);
    Token token = expectRetrieve(NonTerminal.RET_TYPE);
    exitRule(NonTerminal.RET_TYPE);
    return Type.newPrimitiveType(token.getLexeme());
  }

  // type := "int" | "char" | "string" | "array" NUMBER "of" type
  private Type type() throws ParserException {
    enterRule(NonTerminal.TYPE);

    if (accept(TokenKind.INT)) { return Type.newPrimitiveType("int");}
    else if (accept(TokenKind.CHAR)) {return Type.newPrimitiveType("char");}
    else if (accept(TokenKind.STRING)) {return Type.newPrimitiveType("string");}
    else if (accept(TokenKind.ARRAY)) {
    	Token numberToken;
      numberToken = expectRetrieve(TokenKind.NUMBER);
      expect(TokenKind.OF);
      return Type.newArrayType(type(), Integer.parseInt(numberToken.getLexeme()));
    }

    exitRule(NonTerminal.TYPE);
    
    //not used due to program structure but needed by Java
    return null;
  }

  // declarations := { "const" IDENT "=" NUMBER ";" | "var" IDENT { "," IDENT } ":" type ";" }
  private void declarations() throws ParserException {
    enterRule(NonTerminal.DECLARATIONS);
    
    Type temp = null;
    ArrayList<Token> idents = new ArrayList<Token>();
    String constValue = null;
    
    while (have(NonTerminal.DECLARATIONS)) {
      if (accept(TokenKind.CONST)) {
        idents.add(expectRetrieve(TokenKind.IDENT));
        expect(TokenKind.ASSIGN);
        constValue = expectRetrieve(TokenKind.NUMBER).getLexeme();
        expect(TokenKind.SEMICOLON);

        Symbol constSymbol =  Symbol.newConstSymbol(idents.get(0).getLexeme(), Type.newPrimitiveType("int"), constValue);
        tryDeclareSymbol(idents.get(0),constSymbol);

        idents.clear();

      } else if (accept(TokenKind.VAR)) {
        idents.add(expectRetrieve(TokenKind.IDENT));
        while (accept(TokenKind.COMMA)) idents.add(expectRetrieve(TokenKind.IDENT));
        expect(TokenKind.COLON);
        temp = type();
        expect(TokenKind.SEMICOLON);

        Token token;
        Iterator<Token> i = idents.iterator();
        while(i.hasNext())
        {
        	 token = i.next();
           tryDeclareSymbol(token, getSymbolForType(token, temp));
 
        }   
        idents.clear();
        }
        else expect(NonTerminal.DECLARATIONS);
    }
    
    exitRule(NonTerminal.DECLARATIONS);
  }

  // procedureFormalParams := type IDENT { "," type IDENT }
  private void procedureFormalParams(Symbol symbol, Token inToken) throws ParserException {
    enterRule(NonTerminal.PROCEDURE_FORMAL_PARAMS);

    do {
      Type type = type();
      
      if(type instanceof Type.ArrayType)
      	printError(ErrorType.MISMATCH_ERROR, inToken, "Invalid argument: " + scanner.getLexeme() + ", " + type );
      	
      Token token = expectRetrieve(TokenKind.IDENT);
      
      tryDeclareSymbol(token, getSymbolForType(token, type));
      
      //adds parameters to arraylist<symbol> for procedure kind.
      symbol.addParam(token.getLexeme(), type);
      
      
    } while (accept(TokenKind.COMMA));

    exitRule(NonTerminal.PROCEDURE_FORMAL_PARAMS);
  }

  // procedureDeclarations := { retType IDENT "(" [ procedureFormalParams ] ")" "{"
  //                            declarations procedureDeclarations statementSequence "}" }
  private void procedureDeclarations() throws ParserException {
    enterRule(NonTerminal.PROCEDURE_DECLARATIONS);

    while (have(NonTerminal.PROCEDURE_DECLARATIONS)) {
      Type retType = retType(); 
      Token ident = expectRetrieve(TokenKind.IDENT);
      Type returned;
      
      this.tryDeclareSymbol(ident, Symbol.newProcedureSymbol(ident.getLexeme(), retType));
      
      Symbol symbol = this.tryResolveSymbol(ident, Symbol.Kind.PROCEDURE);
      
      expect(TokenKind.L_PAREN);
      symbolTable.newScope();
      if (have(NonTerminal.PROCEDURE_FORMAL_PARAMS)) procedureFormalParams(symbol, ident);
      expect(TokenKind.R_PAREN);
      expect(TokenKind.L_BRACE);
      if (have(NonTerminal.DECLARATIONS)) declarations();
      if (have(NonTerminal.PROCEDURE_DECLARATIONS)) procedureDeclarations();
      
      returned = statementSequence();
      
      
      if(returned == null) //do nothing. Don't handle the case where return was missing;
      {
      	
      }
      else
      {
      	if(retType.equals("void"))
      		printError(ErrorType.MISMATCH_ERROR, returnToken, "Invalid return: expected = " + retType + ", received = " + returned);
      	if(!retType.equals(returned))
      	{ 
      		printError(ErrorType.MISMATCH_ERROR, returnToken, "Invalid return: expected = " + retType + ", received = " + returned);
      	}
      }
      expect(TokenKind.R_BRACE);
      symbolTable.exitScope();
    }
    returnToken = null;
    exitRule(NonTerminal.PROCEDURE_DECLARATIONS);
  }

  // program := declarations procedureDeclarations
  //            "main" "(" ")" "{" declarations statementSequnce "}"
  private void program() throws ParserException {
    enterRule(NonTerminal.PROGRAM);

    if (have(NonTerminal.DECLARATIONS)) declarations();
    
    if (have(NonTerminal.PROCEDURE_DECLARATIONS))
    {
    	procedureDeclarations();
    }
    // Special case for main()
    expect(TokenKind.MAIN);
    expect(TokenKind.L_PAREN);
    symbolTable.newScope();
    expect(TokenKind.R_PAREN);
    expect(TokenKind.L_BRACE);
    if (have(NonTerminal.DECLARATIONS)) declarations();
    
    Type type = statementSequence();
    
    if(type != null)
    {
    	printError(ErrorType.MISMATCH_ERROR, returnToken, "Invalid return: expected = void" + ", received = " + type );
    }
    expect(TokenKind.R_BRACE);

    symbolTable.exitScope();
    
    expect(TokenKind.EOF);

    exitRule(NonTerminal.PROGRAM);
  }
  
  public Symbol getSymbolForType(Token token, Type type)
  {
			return Symbol.newVarSymbol(token.getLexeme(), type);
  }
  public Type getBaseType(Symbol type, int depth)
  {
  	Type baseType = type.getType();
  
  	for(int i = 0; i < depth; i++)
  	{ 
  		baseType = ((Type.ArrayType)baseType).getBase();
  	}
  	return baseType;
  }
}